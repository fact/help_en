<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="savgol_en" 
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>savgol</refname>

    <refpurpose> smoothing, first or second derivate of spectra according to Savitzsky-Golay </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>[result,SG_filter] = savgol(x,window,deriv(degree)) </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
    
      <varlistentry>
         <term> x: </term>   
          <listitem> <para> a Div structure or a matrix of dimensions (n x q)  </para> </listitem> </varlistentry>       
 
     <varlistentry>
        <term> window: </term>
          <listitem> <para> the window for the calculation; an odd integer >=3  </para>
          </listitem> </varlistentry> 

   <varlistentry>
        <term> deriv: </term>
          <listitem> <para> the derivative: 0 (smoothing), 1 (first derivative) or 2 (second derivative) </para>             
          </listitem> </varlistentry> 


     <varlistentry>
        <term> (degree): </term>
          <listitem> <para> the degree of the polynom; an integer between 2 and 5 ;by default degree=2 </para>             
          </listitem> </varlistentry> 

      <varlistentry>
        <term> result: </term>
          <listitem> <para> the spectra obtained after the 0, 1 or 2nd derivative, into a Div structure  </para>
          <para> result.d is a matrix of dimensions (n x q) </para>
          </listitem> </varlistentry>
 
       <varlistentry>
        <term> SG_filter: </term>
        <listitem> <para> a matrix such that: result = x * SG_filter   </para>
          <para> SG_filter.d is a matrix of dimensions (q x q) </para>
          <para> Note: the calculation (x*SG_filter) is less precise than the first output (result) at less than window/2 from the edges of the spectra </para>
          </listitem> </varlistentry> 
 
           
     </variablelist>                 
  </refsection>

  <refsection>
    <title>Examples</title>    
   <programlisting role="example">[result,f_sg]=savgol(x,15,1)              </programlisting>
  </refsection>

 <refsection>
    <title>Bibliography</title>
     <simplelist type="vert">
      <member> Savitzsky, Golay, Smoothing and differentiation of data by simplified least squares procedures, Anal. Chemistry, 1964  </member>
     </simplelist>
  </refsection>


  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>JC Boulet, INRA  </member>
    </simplelist>
  </refsection>
  
</refentry>









<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="obiwarp_en"
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>obiwarp</refname>

    <refpurpose> correction of retention times in liquid and gaz chromatography</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>x2_new_rt=obiwarp(x1_master,x2_slave,score,local,nostdnrm,factors,gaps,init_penalty,response) </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
    
    <varlistentry>
        <term> x1_master: </term>
        <listitem> <para> a Div structure or a matrix of dimensions (n1 x q)  </para> </listitem> </varlistentry>
    <varlistentry>
        <term> x2_slave: </term>
        <listitem> <para> a Div structure or a matrix of dimensions (n2 x q)  </para> </listitem> </varlistentry>
    
    <varlistentry>
        <term> score: </term>
        <listitem> <para> the score function; 4 possibilities: </para>
            <para>'cor': Pearson correlation coefficient (by default) </para>
            <para> 'cov' : covariance  </para>
            <para> 'prd' : dot product </para>
            <para> 'euc' : Euclidian distance </para>
        </listitem> </varlistentry>
    
    <varlistentry>
        <term> local: </term>
        <listitem> <para> local alignement rather than global (by default) </para>
        </listitem> </varlistentry>
    
    <varlistentry>
        <term> nostdnrm: </term>
        <listitem> <para> no standard normalization; by default the score matrix values are mean centered and divided by the standard deviation (unless all values are equal) </para>
        </listitem> </varlistentry>
    
    <varlistentry>
        <term> factors: </term>
        <listitem> <para> local weighting applied to diagonal/gap moves in alignment; two numbers between entre brackets separated by a comma  </para>
            <para> ex: [2,1] gives an unbiased alignment</para>
        </listitem> </varlistentry>
    
    <varlistentry>
        <term> gaps: </term>
        <listitem> <para> gap penalties, initiate then extend; two numbers between brackets separated by a comma    </para>
            <para> the default values are:  </para>
            <para> 'cor'-> [0.3, 2.4]   </para>
            <para> 'cov' -> [0,11.7]    </para>
            <para> 'prd' -> [0,7.8]    </para>
            <para> 'euc' ->  [0.9,1.8]   </para>
        </listitem> </varlistentry>
    
    <varlistentry>
        <term> init_penalty: </term>
        <listitem> <para> penalty for initiating alignment (for local alignment only) </para>
            <para> by default: 0   </para>
        </listitem> </varlistentry>
    
    <varlistentry>
        <term> response: </term>
        <listitem> <para> responsiveness of warping; 0 will give a linear warp based on start and end points; 100 will use all bijective anchors  </para>
            <para>  </para>
            <para> </para>
        </listitem> </varlistentry>
    
    <varlistentry>
        <term> x2_new_rt: </term>
        <listitem> <para> a vector of dimensions (n2 x 1) </para>
        </listitem> </varlistentry>
           
     </variablelist>                 
  </refsection>

  <refsection>
    <title>Examples</title>    
   <programlisting role="example"> x2_new_rt=obiwarp(x1,x2,'cov',[],[],[2.1,1],[0,11.7],0,30);        </programlisting>
  </refsection>

 <refsection>
    <title>Bibliography</title>
     <simplelist type="vert">
      <member> Prince, Marcotte, Chromatographic aligment of ESI-LC-MS proteomics data sets by ordered bijective interpolated warping, Anal. Chemistry, 2006   </member>
     </simplelist>
  </refsection>


  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>http://obi-warp.sourceforge.net/ </member>
    </simplelist>
  </refsection>
  
</refentry>









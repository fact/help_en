<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="pop_dtune_en" 
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>pop_dtune</refname>

    <refpurpose>calculates the tuning parameters of an orthogonal projection for which the matrix of detrimental information is given   </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>res_dmt=pop_tune(dmatrix,maxdim,xg,classes_observ,func_reg,x,y,split,lv,(rparam),(centering)) </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
  
    <varlistentry>
         <term> dmatrix: </term>   
          <listitem> <para> a matrix (n1 x q) or a Div structure containing only detrimental information 
          </para> </listitem> </varlistentry>         
  
    <varlistentry>
         <term> maxdim: </term>   
          <listitem> <para> the maximum number of dimensions extracted from dmatrix 
          </para> </listitem> </varlistentry>            
 
   <varlistentry>
         <term> (xg): </term>   
          <listitem> <para> a matrix or a Div structure containing  detrimental information  
          </para> </listitem> </varlistentry>   

    <varlistentry>
         <term> (classes_echant): </term>   
          <listitem> <para> a vector identifying the samples in xg   
          </para> </listitem> </varlistentry>    

   <varlistentry>
         <term> func_reg: </term>   
          <listitem> <para> the chosen regression function; a string, ex: 'pls' 
          </para> </listitem> </varlistentry>

    <varlistentry>
         <term> x,y: </term>   
          <listitem> <para> a calibration dataset: a matrix and a vector or Div structures   
          </para> </listitem> </varlistentry>
          
        <varlistentry>
         <term> split: </term>   
          <listitem> <para> for the cross-validation; the number of blocks, or a vector attributing each observation to a CV block   </para> 
          </listitem> </varlistentry>         
          
        <varlistentry>
         <term> lv: </term>   
          <listitem> <para> number of latent variables for the regression  </para> 
          </listitem> </varlistentry>  
          
      <varlistentry>
        <term> (rparam): </term>
          <listitem> <para> the r parameter; only for the function 'vodka'</para> 
          </listitem> </varlistentry>                   
          
         <varlistentry>
         <term> (centering): </term>   
          <listitem> <para> centred = 1 (by default); not centred=0  </para> 
          </listitem> </varlistentry>        

      <varlistentry>
        <term> res.d_matrix: </term>
          <listitem> <para> the matrix containing only detrimental information  </para> 
          <para> res.d_matrix.d is a matrix of dimensions ((nbr_perturb-1) x q)</para> 
          </listitem> </varlistentry> 

      <varlistentry>
        <term> res.d_eigenvect: </term>
          <listitem> <para> the eigenvectors of d_matrix  </para> 
          <para> res.d_eigenvect.d is a matrix of dimensions (q x (nbr_perturb-1))</para> 
          </listitem> </varlistentry> 

      <varlistentry>
        <term> res.d_eigenvalpcent: </term>
          <listitem> <para> the eigenvalues of d_matrix in percent  </para> 
          <para> res.d_eigenvalpcent.d is a vector of dimensions ((nbr_perturb-1) x 1) </para> 
          </listitem> </varlistentry> 

      <varlistentry>
        <term> res.wilks: </term>
          <listitem> <para> Wilks lambda </para>  
          <para> res.Wilks.d is a vector of dimensions (nbr_perturb x 1)  </para>
          </listitem> </varlistentry> 

      <varlistentry>
        <term> res.rmsecv: </term>
          <listitem> <para> rmsecv for several dimensions of orthogonal projection and several of regression  </para>  
          <para> res.rmsecv.d is a matrix of dimensions (lv x nbr_perturb)  </para>
          </listitem> </varlistentry> 

        <varlistentry>
         <term> res.pls_models: </term>   
          <listitem> <para> regression models obtained after an orthogonal projection using 0/1/2/...(nbr_perturb - 1) eigenvectors of res.eigenvect.d  </para>  
          <para> res.pls_models is a list of dimension (nbr_perturb) </para>
          <para> help pls for more information about the fields of res.pls_models </para>
          </listitem> </varlistentry>

     </variablelist>                 
  </refsection>

  <refsection>
    <title>Examples</title>    
   <programlisting role="example">[res]=pop_dtune(d,20,x0, x0_cl_ech,'pls',xcal,ycal,10,5)               </programlisting>
   <programlisting role="example">[res]=pop_dtune(d,20,x0,x0_cl_ech,'pls',xcal,ycal,10,5,0)          </programlisting>
    <programlisting role="example">[res]=pop_dtune(d,20,'pls',xcal,ycal,10,5,0)          </programlisting>
  </refsection>


  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>JC Boulet, INRA  </member>
    </simplelist>
  </refsection>
  
</refentry>

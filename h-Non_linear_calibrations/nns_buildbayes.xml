<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="nns_buildbayes_en"
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>nns_buildbayes</refname>

    <refpurpose>building of a neural network with 1 hidden layer and with bias </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>[result]=nns_buildbayes(wh_in,wo_in,x,y, (options))</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
     <variablelist>                        

        <varlistentry>   <term> wh_in: </term>   
             <listitem> <para>  coefficients of the hidden neurons + bias, obtained from the initialization step       </para> 
			<para>  wh_in is a matrix of dimensions ((q+1) x nh) or a Div  structure        </para>
			<para>  nh is the number of hidden neurons        </para>
		</listitem>  </varlistentry>      

        <varlistentry>   <term> wo_in: </term>
             <listitem> <para> coefficients of the output neurons + bias, obtained from the initialization step   </para> 
			<para> wo_in is a matrix of dimensions ((nh+1) x no) or a Div  structure   </para>
			<para> no is the number of output neurons   </para>
		</listitem>  </varlistentry>

       <varlistentry>   <term> x: </term>   
             <listitem> <para>  calibration dataset     </para> 
			<para>  x is a matrix of dimensions (n x q) or a Div structure       </para>
		</listitem>  </varlistentry>      

     <varlistentry>   <term> y: </term>
             <listitem> <para> reference values, to be predicted  </para> 
			<para> y is a matrix of dimensions (n x no) or a Div structure  </para>
		</listitem>  </varlistentry>


    <varlistentry>   <term> (options_in): </term>
             <listitem>  
		 <para>options for the building of the model  </para> 
                 <para>options_in.maxtime: max. run time, in seconds (default=300)  			</para> 
                 <para>options_in.maxiter: max. number of iterations (default=10000) 			</para>
                 <para>options_in.displayfreq: gap between two successive iterations to be plotted in a figure (default=10) 			</para>  
                 <para>options_in.precresid: accuracy of the residuals; a scalar or a vector of length no (default=1e-6*stdout) 			</para>
                 <para>options_in.precparam: accuracy of the parameters; a scalar or a vector of length nc (default=1e-4)  			</para> 
		 <para>options_in.stdresmin: minimum of the standard errors of the residuals; a scalar or a vector of length no (default=1e-6*stdout)	</para>                
		 <para>options_in.stdresmax: maximum of the standard errors of the residuals; a scalar or a vector of length no (default=0.1*stdout)    </para> 
		 <para>options_in.regclass: weights regularization   										</para> 
		 <para>… 0 = no regularization  												</para> 
		 <para>… 1 = a class for all weights 												</para> 
		 <para>… 2 = two classes: one for the inputs, the other for the outputs   							</para> 
		 <para>… 3 = a class for each input, a class for the bias of the hidden layer and a class for each output (by default) 	</para> 
		 <para>… 4 = a class for each weight 												</para> 
		 <para> options_in.preproc: preprocessings before learning 									</para> 
		 <para>… 0 = no preprocessing  												</para> 
		 <para>… 1 = normalization (sum of squares of the differences between the inputs and the outputs = n)  			</para> 
		 <para>… 2 = standardization (inputs mean = outputs mean = 0, sum of squares of the residuals = n)				</para> 
		 <para> options_in.momentparam: tuning of the effective number in each class; between 0 and 1 (default = 0.8)	</para> 
	       </listitem>  
    </varlistentry>

   <varlistentry>   <term> result.wh_out: </term>
             <listitem> <para> coefficients of the hidden neurons + bias, after model calculation  					</para> 
			 <para> result.wh_out is a matrix of dimension ((q+1) x no) or a Div structure 					</para>
			<para> nh is the number of hidden neurons  						   		      	</para>	
	</listitem>  </varlistentry>

   <varlistentry>   <term> result.wo_out: </term>
             <listitem> <para> coefficients of the output neurons  + biais, after model calculation 					</para> 
			<para> result.wo_out is a matrix of dimensions ((nh + 1) x no) or a Div structure 	  				</para>
			<para> no is the number of output neurons  						  				</para>
		</listitem>  </varlistentry>

   <varlistentry>   <term> result.stdres: </term>
             <listitem> <para> estimation of the standard error of the residuals 		 						</para> 
			<para> result.stdres is a Div structure   										</para>
			<para> result.stdres.d is a vector of dimensions (1 x no) 				  				</para>
		</listitem>  </varlistentry>

   <varlistentry>   <term> result.covw: </term>
             <listitem> <para> estimation of the variance-covariance matrix for the weights  						</para> 
			<para> result.covw is a Div structure  										</para>
			<para> result.covw.d is a matrix of dimensions (nw x nw)				  				</para>
		</listitem>  </varlistentry>

   <varlistentry>   <term> result.options_out: </term>
             <listitem>  
		 <para>options_in with the following fields:  </para> 
                 <para>result.options_out.stop: a string message about the learning stop 							</para> 
                 <para>result.options_out.r2: square correlation coefficient for each output          						</para>
                 <para>result.options_out.wheff: ratio number of parameters effective/total for each weight of the hidden layer 			</para>  
                 <para>result.options_out.wheff.d is a matrix of dimensions ((q+1) x nh)					 			</para>
                 <para>result.options_out.woeff: ratio number of parameters effective/total for each weight of the output layer			</para> 
		 <para>result.options_out.woeff.d is a matrix of dimensions ((nh+1) x no)								</para>                
		 <para>result.options_out.histiters: number of iterations									      	</para> 
		 <para>result.options_out.histresid: residual errors for each iteration		  						</para>
		 <para>result.options_out.histresid.d is a matrix of no lines and as columns as iterations 		 			</para> 
		 <para>result.options_out.histparam: effective number of parameters, for each class							</para> 
		 <para>result.options_out.histparam.d has as coulons as classes and as lines as iterations  					</para> 
		 <para>result.options_out.classnumbers: number of class for each weight, ranked in a vector according to [wh(:);wo(:)]		</para> 
		 <para>result.options_out.totalparam: total number of parameters (weights) in each class of weights 			 	</para> 
		 <para>result.options_out.totalparam.d is a line vector with as columns as classes 							</para> 
	       </listitem>  
    </varlistentry> 
  
      </variablelist>            
  </refsection>

  <refsection>
    <title>Examples</title>    
    <programlisting role="example">[res]=nns_buildbayes(wh,wo,x,y)                                  </programlisting>  
  </refsection> 
  
  <refsection>
    <title>Bibliography</title>
     <simplelist type="vert">
      <member>  </member>
     </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>IC Trelea, AgroParisTech   </member>
    </simplelist>
  </refsection>
  
</refentry>





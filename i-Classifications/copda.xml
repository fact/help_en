<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="copda_en" 
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>copda</refname>

    <refpurpose> classification by orthogonal projection (COP) with cross-validation  </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>model = copda(x,y_class,split,lv,(metric),(scale),(threshold) ) </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
         <term> x: </term>   
             <listitem> <para> a matrix (n x q) or a Div structure  
             </para> </listitem> </varlistentry>       

      <varlistentry>
        <term> y_class: </term>
          <listitem> <para> a conjunctive vector (n x 1) or a disjonctive matrix (n x nclass) or a Div structure  </para>                   
          </listitem> </varlistentry>  
          
      <varlistentry> 
        <term> split: </term> 
          <listitem> <para> for the cross-validation; the number of blocks or a vector of dimensions (n x 1) identifant by a number the group of cross-validation of each observation </para> 
          </listitem> </varlistentry>       
 
       <varlistentry>
         <term> lv: </term>   
             <listitem> <para> maximum number of latent variables or eigenvectors used for the building of the model </para>
              <para> lv inf. or equal to nclass  </para>
            </listitem> </varlistentry>  
   
       <varlistentry>
         <term> (metric): </term>   
             <listitem> <para> the metric used for the calculation of the distances between the observations and the samples  </para>
             <para> metric=0:  Mahalanobis distance (by default) </para>
             <para> metric=1:  usual Euclidian distance   </para>
             </listitem> </varlistentry> 
   
      <varlistentry>
         <term> (scale): </term>   
             <listitem> <para> the columns of x are always centered; scale defines the standardization </para>
             <para> scale='c':  no standardization </para>
             <para> scale='cs':  standardisation; the columns of x are divided by their standard deviation (by default)   </para>
             </listitem> </varlistentry>
   
       <varlistentry>
         <term> (threshold): </term>   
             <listitem> <para> the lowest value for attributing an observation to a class (by default= 1/nclass )  </para>
             </listitem> </varlistentry> 

       <varlistentry>
         <term> model.conf_cal_nobs: </term>   
             <listitem> <para> the confusion matrix obtained with the calibration models,expressed as numbers of observations  </para>
             <para> model.conf_cal.d is an hyper-matrix of dimensions  (nclass x nclass x lv) </para> 
             </listitem> </varlistentry>  
 
      <varlistentry>
         <term> model.conf_cal: </term>   
             <listitem> <para> the confusion matrix obtained with the calibration models, expressed as percentages  </para>
             <para> model.conf_cal.d is an hyper-matrix of dimensions  (nclass x nclass x lv) </para> 
             </listitem> </varlistentry>   

       <varlistentry>
         <term> model.conf_cv: </term>   
             <listitem> <para> the confusion matrix obtained with the cross-validations, expressed as percentages </para>
             <para> model.conf_cv.d is an hypermatrix of dimensions (nclass x nclass x lv) </para> 
             </listitem> </varlistentry>   
   
                <varlistentry>
         <term> model.err: </term>   
             <listitem> <para> the percentage of classification errors for calibrations (1st column) and cross-validations (2nd column) </para>
             <para> model.err.d is a matrix (lv x 2)</para> 
             </listitem> </varlistentry> 
             
       <varlistentry>
         <term> model.errbycl_cal: </term>   
             <listitem> <para> the percentage of calibration error, for each class  </para>
             <para> model.errbycl_cal.d is a matrix (lv x nclass) </para> 
             </listitem> </varlistentry>   
   
        <varlistentry>
         <term> model.errbycl_cv: </term>   
             <listitem> <para> the percentage of cross-validation error, for each class   </para>
             <para> model.errbycl_cv.d is a matrix (lv x nclass) </para> 
             </listitem> </varlistentry> 

        <varlistentry>
         <term> model.notclassed: </term>   
             <listitem> <para> the percentage of not-classed observations (all predictions lower than the threshold) </para>
             <para> model.notclassed.d is a vector (lv x 1) </para> 
             </listitem> </varlistentry>   
   
        <varlistentry>
         <term> model.notclassed_bycl: </term>   
             <listitem> <para> the percentage of not-classed observations (all predictions lower than the threshold), for each class </para>
             <para> model.notclassed_bycl.d is a matrix (lv x nclass) </para> 
             </listitem> </varlistentry> 
 
        <varlistentry>
         <term> model.method: </term>   
             <listitem> <para> the discriminant method; here: 'copda' </para>
            </listitem> </varlistentry>              
                         
        <varlistentry>
         <term> model.loadings: </term>   
             <listitem> <para> the eigenvectors </para>
             <para> model.eigenvec.d is a matrix of dimensions (q x lv) </para>
             </listitem> </varlistentry>  
 
         <varlistentry>
         <term> model.classif_metric: </term>   
             <listitem> <para> the metric used for the calculation of the distances between observations and groups  </para>
             </listitem> </varlistentry> 
 
         <varlistentry>
         <term> model.scale: </term>   
             <listitem> <para> applied standardization </para>
             </listitem> </varlistentry> 
 
         <varlistentry>
         <term> model.threshold: </term>   
             <listitem> <para> applied threshold  </para>
             </listitem> </varlistentry>  

           
     </variablelist>                 
  </refsection>

  <refsection>
    <title>Examples</title>    
    <programlisting role="example">[result1]=copda(x,y,30,20)                </programlisting>
    <programlisting role="example">[result1]=copda(x,y,30,20,1,'cs',0.1)        </programlisting>
  </refsection>

  <refsection>
    <title>Bibliography</title>
     <simplelist type="vert">
      <member>JM Roger, 2013, submitted </member>
      </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>JM Roger, IRSTEA  </member>
    </simplelist>
  </refsection>
  
</refentry>

<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="vodka_en" 
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>vodka</refname>

    <refpurpose> the Vodka regression  </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>model=vodka(x,y,split,lv,r,centred)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
     <variablelist>                        

        <varlistentry>   <term> x and y : </term>   
             <listitem> <para>  the calibration dataset; a matrix (n x q) and a vector (n x 1) or Div structures    </para> </listitem>  </varlistentry>   

         <varlistentry>   <term> split: </term>
             <listitem> <para> parameter for the cross-validation:  </para> 
          <para>  - an integer:  number of random blocs   </para> 
        <para>  - two integers [a b]: a random blocks ; b repetitions of the cross-validation </para>
         <para>  - a vector of dimension n attributing each observation to a block (numbers 1,2,...k for k blocks): blocks given by the vector </para>  
        <para>  - a matrix (n x b ) of column-vectors of dimension n attributing each observation to a block (numbers 1,2,...k for k blocs): blocks given by each vector, b repetitions of the cross-validation   </para>
            <para>  - 'vnbxx':  venitian blinds, xx blocks; ex: 'vnb10' for 10 blocks  </para> 
         <para>  - 'jckxx':  Jack knife, xx blocks; ex: 'jck8' for 8 blocks  </para> 
        </listitem>  </varlistentry>              

        <varlistentry>   <term> lv : </term>
             <listitem> <para> number of latent variables 
           </para> </listitem>  </varlistentry>

        <varlistentry>   <term> r : </term>
             <listitem>
                 <para>  a parameter for Vodka; a vector (q x 1) or an integer among {0,1,2}:       </para>
                 <simplelist type="vert">                       
                        <member> r=0 -->  the mean of the columns of x      </member>
                        <member> r=1 -->  x'.y that is standard PLSR     </member>
                        <member> r=2 -->  x'.y2 with y2 = square of y    </member>   
                 </simplelist> </listitem>  </varlistentry>
 
        <varlistentry>  <term> (centred): </term> 
             <listitem> <para> centering =1 (by default); no centering = 0  </para> </listitem> </varlistentry>

       <varlistentry>   <term> model.rmsec: </term>
           <listitem> <para> the root mean square error of calibration        </para> 
                      <para> model.rmsec.d is a vector of dimension (lv x 1)                              </para> </listitem> </varlistentry>

        <varlistentry>   <term> model.err: </term>
           <listitem> <para> the standard errors of calibration aand cross-validation  </para> 
                      <para> model.err.d is a matrix (lv x 2); the columns are the rmsec and the rmsecv respectively                               </para> </listitem> </varlistentry>

       <varlistentry>   <term> model.b: </term>
             <listitem> <para> the b coeficients or regression coefficients                          </para>                           
             <para> model.b.d is a matrix (q x lv)                                          </para> </listitem> </varlistentry>   

       <varlistentry>   <term> model.scores: </term>
             <listitem> <para>the scores of the observations onto the loadings      </para>                      
             <para> model.scores.d is a matrix of dimensions (n x lv)                                          </para> </listitem> </varlistentry>
             
       <varlistentry>   <term> model.loadings: </term>
             <listitem> <para>the loadings       </para>                           
             <para> model.loadings.d is a matrix of dimensions (q x lv)                                          </para> </listitem> </varlistentry>

       <varlistentry>   <term> model.x_mean, model.y_mean: </term>      
             <listitem> <para>means of x and y; a vector (q x 1) and a scalar                   </para> </listitem> </varlistentry> 

       <varlistentry>   <term> model.center: </term>
             <listitem> <para> 1 = centred; 0 = not centred  </para> </listitem> </varlistentry>
       <varlistentry>   <term> model.x_mean, model.y_mean: </term>      
             <listitem> <para>means of x and y; a vector (q x 1) and a scalar                  </para> </listitem> </varlistentry> 

      <varlistentry>   <term> rmsec: </term>
           <listitem> <para> rmsec=model.rmsec.d   </para> 
            </listitem> </varlistentry>

       <varlistentry>   <term> rmsecv: </term>
             <listitem> <para>rmsecv=model.rmsecv.d       </para>
        </listitem> </varlistentry>

       <varlistentry>   <term> b: </term>
             <listitem> <para>b=model.b.d   </para>  
             </listitem> </varlistentry>   

       <varlistentry>   <term> ypredcv: </term>
             <listitem> <para> ypredcv=model.ypredcv.d   </para> 
             </listitem> </varlistentry>

             
      </variablelist>            
  </refsection>

  <refsection>
    <title>Examples</title>    
    <programlisting role="example">[model]=vodka(x,y,50,20,1)                                </programlisting>
    <programlisting role="example">[model]=vodka(x,y,50,20,1,0)                              </programlisting>   
    <programlisting role="example">[model]=vodka(x,y,50,20,mean(x,'r'),0)                    </programlisting>
  </refsection> 
  
  <refsection>
    <title>Bibliography</title>
     <simplelist type="vert">
      <member>Boulet et al., A family of regression methods derived from standard PLSR, Chemom.Intell.Lab.Syst.,2012 </member>
     </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>JC Boulet, INRA   </member>
      <member>JM Roger, IRSTEA  </member>
    </simplelist>
  </refsection>
  
</refentry>

<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="ridge_en" 
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>ridge</refname>

    <refpurpose>the Ridge regression </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>model=ridge(x,y,split,lambda,(centred))</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
     <variablelist>                        

        <varlistentry>   <term> x and y : </term>   
             <listitem> <para>  calibration dataset; a matrix (n x q) and a vector (n x 1)or Div structures  
              </para> </listitem>  </varlistentry>   

       <varlistentry>   <term> split: </term>
             <listitem> <para> parameter for the cross-validation:  </para> 
          <para>  - an integer:  number of random blocs   </para> 
        <para>  - two integers [a b]: a random blocks ; b repetitions of the cross-validation </para>
         <para>  - a vector of dimension n attributing each observation to a block (numbers 1,2,...k for k blocks): blocks given by the vector </para>  
        <para>  - a matrix (n x b ) of column-vectors of dimension n attributing each observation to a block (numbers 1,2,...k for k blocs): blocks given by each vector, b repetitions of the cross-validation   </para>
            <para>  - 'vnbxx':  venitian blinds, xx blocks; ex: 'vnb10' for 10 blocks  </para> 
         <para>  - 'jckxx':  Jack knife, xx blocks; ex: 'jck8' for 8 blocks  </para> 
        </listitem>  </varlistentry>                

        <varlistentry>   <term> lambda : </term>
                         <listitem> <para> a positive scalar or a column vector of positive scalars; if lv scalars, lv models are obtained  </para> </listitem> </varlistentry>
 
        <varlistentry>  <term> (centred): </term> 
             <listitem> <para> centred = 1 (by default); not centred = 0 
              </para> </listitem> </varlistentry>

           <varlistentry>   <term> model.err: </term>
           <listitem> <para> the standard errors of calibration aand cross-validation  </para> 
                      <para> model.err.d is a matrix (lv x 2); the columns are the rmsec and the rmsecv respectively                               </para> </listitem> </varlistentry>

       <varlistentry>   <term> model.ypredcv: </term>
             <listitem> <para> the predicted values after cross validation   </para>     
             <para> model.ypredcv.d is a matrix (n x lv)                                    </para> </listitem> </varlistentry>

       <varlistentry>   <term> model.b: </term>
             <listitem> <para>the b coefficients or regression coefficients  </para>           
             <para> model.b.d is a matrix (q x lv)                                          </para> </listitem> </varlistentry>   

       <varlistentry>   <term> model.scores: </term>
             <listitem> <para>the scores of the observations onto the loadings       </para>                           
              </listitem> </varlistentry>
             
       <varlistentry>   <term> model.loadings: </term>
             <listitem> <para>the loadings       </para>                           
             </listitem> </varlistentry>

       <varlistentry>   <term> model.x_mean, model.y_mean: </term>      
             <listitem> <para>means of x and y; a vector (q x 1) and a scalar                   </para> </listitem> </varlistentry> 
 
       <varlistentry>   <term> model.center: </term>
             <listitem> <para> 1=centred; 0 = not centred   </para> </listitem> </varlistentry>
 
      <varlistentry>   <term> rmsec: </term>
           <listitem> <para> rmsec=model.rmsec.d   </para> 
            </listitem> </varlistentry>

       <varlistentry>   <term> rmsecv: </term>
             <listitem> <para>rmsecv=model.rmsecv.d       </para>
        </listitem> </varlistentry>

       <varlistentry>   <term> b: </term>
             <listitem> <para>b=model.b.d   </para>  
             </listitem> </varlistentry>   

       <varlistentry>   <term> ypredcv: </term>
             <listitem> <para> ypredcv=model.ypredcv.d   </para> 
             </listitem> </varlistentry>
 
             
      </variablelist>            
  </refsection>

  <refsection>
    <title>Examples</title>    
    <programlisting role="example">[model]=ridge(x,y,50,[1;0.1;0.01])                                </programlisting>
    <programlisting role="example">[model]=ridge(x,y,50,[1;0.1;0.01],0)                              </programlisting>
  </refsection> 
  
  <refsection>
    <title>Bibliography</title>
     <simplelist type="vert">
     <member> Martens et Naes. Multivariate calibration, Eds Wiley, 1989 </member>
     </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>JC Boulet, INRA   </member>
      <member>JM Roger, IRSTEA  </member>
    </simplelist>
  </refsection>
  
</refentry>

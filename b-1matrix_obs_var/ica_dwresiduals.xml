<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="ica_dwresiduals_en" 
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>ica_dwresiduals</refname>

    <refpurpose>  the number of independent components in ICA is determined from the Durbin-Watson criterium </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>res=ica_dwresiduals(x,n_ics,(options));</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
 
        <varlistentry>
         <term> x: </term>   
             <listitem> <para> a matrix (n x q) or a Div structure  </para>  
           </listitem> </varlistentry>
    
    <varlistentry>
         <term> n_ics: </term>   
             <listitem> <para> maximum number of independent components  </para>   
           </listitem> </varlistentry>       
           
    <varlistentry>
         <term> (options): </term>   
             <listitem> <para> an optional structure containing the following fields:  </para>   
         <para>options.center: 1 for centering, 0 for no centering (by default)  </para>   
      <para> options.method: choice among: 'tall','wide','kernel','comdim' or 'normal' (by default) </para>   
         <para> options.partitions: an integer; for 'tall','wide' and 'comdim' methods only (by default = 1)  </para>  
        
         <para> options.plot: 0 = no plotting (by defautl); 1 = lowest correlations; 2 = all  </para>  
                     
     </listitem> </varlistentry>  
  
 
      <varlistentry>
        <term> res.all_dw: </term>
          <listitem> 
          <para> all the Durbin-Watson values </para> 
          <para> res.all_dw.d is a matrix of dimensions (n_ics x n)  </para> 
          </listitem>
     </varlistentry>  
 
     <varlistentry>
        <term> res.mean_all_dw: </term>
          <listitem> 
          <para> the means of the rows of res.all_dw.d </para>  
          <para> res.mean_all_dw.d is a vector of dimensions (n_ics x 1)  </para> 
          </listitem> 
     </varlistentry>  

     <varlistentry>
        <term> res.residus: </term>
          <listitem> 
          <para> the residues; a list of n_ics Div structures  </para> 
         <para> res.residus(i).d is a matrix of dimensions (n x q)  </para> 
          </listitem> </varlistentry>  

     </variablelist>                 
  </refsection>

  <refsection>
    <title>Examples</title>    
    <programlisting role="example"> [my_result]=ica_dwresiduals(x,5,my_options);      </programlisting>
  </refsection>


  <refsection>
    <title>Bibliography</title>
     <simplelist type="vert">
      <member> Jouan-Rimbaud, Moya-Gonzales, Ammari, Rutledge. Two novel methods for the determination of the number of components in independant components analysis models, Chem.Intell.Lab.Syst., 2012 </member>
     </simplelist>
  </refsection>


  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
     <member>D.N. Rutledge and D. Jouan-Rimbaud Bouveresse, AgroParisTech, FR </member>
    </simplelist>
  </refsection>
  
</refentry>

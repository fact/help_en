<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - Allan CORNET
 * 
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" 
          xml:id="ica_blocs_signals_en" 
          xml:lang="en"
          xml:space="preserve"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>ica_blocs_signals</refname>

    <refpurpose>  the number of independent components is determined from the correlations between blocs issued from the dataset </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling sequence</title>

    <synopsis>res=ica_blocs_signals(x,n_ics,split,(options));</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
 
        <varlistentry>
         <term> x: </term>   
             <listitem> <para> a matrix (n x q) or a Div structure  </para>  
           </listitem> </varlistentry>
    
    <varlistentry>
         <term> n_ics: </term>   
             <listitem> <para> maximum number of independent components  </para>   
           </listitem> </varlistentry>       
           

     <varlistentry>
         <term> split: </term>   
             <listitem> 
             <para> choice of the method for splitting x, and n_b = the number of blocs  </para>   
             <para> n_b (integer):  random; n_b blocs  </para>
             <para> 'jckn_b' (n_b=integer): Jack knife = contiguous blocs; n_b blocs; ex: 'jck5'  </para>
             <para> 'vnbn_b' (n_b=integer): Venitian blinds; n_b blocs; ex: 'vnb5'  </para>
             <para> v (vector): a conjunctive vector of length n, identifying the observation to the blocs by the numbers 1 to n_b </para>
             </listitem> 
     </varlistentry> 


    <varlistentry>
         <term> (options): </term>   
             <listitem> <para> an optional structure designed for the tuning fields of the ICA (see icascores help) and the field ibbplot for the figures:  </para>   
                 <para> options.ibbplot: 0 = no plot (by default); 1 = significant + lowest correlations; 2 = all  </para>  
                     
     </listitem> </varlistentry>  
  
 
      <varlistentry>
        <term> res.signal: </term>
          <listitem> 
          <para> the obtained signals for i=1 to n_b blocs and j=1 to n_ics independent components </para> 
          <para> res.signal is a cell of dimensions (n_b x n_ics)  </para> 
          <para> res.signal(i,j).entries is a Div structure </para>
          <para> res.signal(i,j).entries.d is a vector or matrix of dimensions (q x j) </para>
          </listitem>
     </varlistentry>  
 
     <varlistentry>
        <term> res.scores: </term>
          <listitem> 
          <para> the scores obtained for i=1 to n_b blocs and j=1 to n_ics independent components </para>  
          <para> res.scores is a cell of dimensions (n_b x n_ics)  </para> 
          <para> res.scores(i,j).entries is a Div structure  </para>
          <para> res.scores(i,j).entries.d is a vector or matrix with j columns and a number of observations depending on the splitting  </para>              
          </listitem> 
     </varlistentry>  

     <varlistentry>
        <term> res.signal_corr: </term>
          <listitem> 
          <para> the absolute value of the simple correlation coefficients between the signals obtained for the n_b blocs and j=1 to n_ics independent components  </para> 
         <para> res.signal_corr is a cell of dimensions (1 x n_ics)  </para> 
         <para> res.signal_corr(1,j).entries is a vector of dimensions ((a*j)^2,1) </para>
          </listitem> </varlistentry>  
 
 
      <varlistentry>
        <term> res.scores_corr: </term>
          <listitem> 
          <para> the absolute value of the simple correlation coefficients between the scores obtained for the n_b blocs and j=1 to n_ics independent components  </para> 
         <para> res.scores_corr is a cell of dimensions (1 x n_ics)  </para> 
         <para> res.scores_corr(1,j).entries is a vector of dimensions ((n_b*j)^2,1) </para>
          </listitem> 
     </varlistentry>   
     
     <varlistentry>
        <term> res.rmin: </term>
          <listitem> 
            <para> number of ICs and minimal correlations; a Div structure </para>   
            <para> res.rmin.d is a matrix of dimensions (n_ics x 2)       </para>             
          </listitem> 
    </varlistentry> 

     </variablelist>                 
  </refsection>

  <refsection>
    <title>Examples</title>    
    <programlisting role="example"> [my_result]=ica_blocs_signals(x,5,'vnb4',my_options);      </programlisting>
    <programlisting role="example"> [my_result]=ica_blocs_signals(x,5,4);                      </programlisting>
  </refsection>


  <refsection>
    <title>Bibliography</title>
     <simplelist type="vert">
      <member> Jouan-Rimbaud, Moya-Gonzales, Ammari, Rutledge. Two novel methods for the determination of the number of components in independant components analysis models, Chem.Intell.Lab.Syst., 2012 </member>
     </simplelist>
  </refsection>


  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
     <member>D.N. Rutledge and D. Jouan-Rimbaud Bouveresse, AgroParisTech, FR </member>
    </simplelist>
  </refsection>
  
</refentry>
